package com.nearpin.testtaskinteresnee.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import timber.log.Timber

//the feature to use fragments most simply
abstract class ViewBindingFragment<ViewBinding : ViewDataBinding> : Fragment(){

    protected val TAG = javaClass.simpleName

    protected lateinit var binding : ViewBinding

    protected abstract fun getLayoutId(): Int

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        Timber.tag("FragmentLifecycle").d("${this.javaClass.simpleName} : onCreateView")
        binding = DataBindingUtil.inflate(inflater, getLayoutId(), container, false)
        binding.lifecycleOwner = viewLifecycleOwner

        return binding.root
    }
}