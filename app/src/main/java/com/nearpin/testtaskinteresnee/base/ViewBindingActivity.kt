package com.nearpin.testtaskinteresnee.base

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding

//the feature to use Activity most simply
abstract class ViewBindingActivity<ViewBinding: ViewDataBinding> : AppCompatActivity(){

    protected abstract fun getLayoutId(): Int

    protected lateinit var binding: ViewBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(
            this,
            getLayoutId()
        )
    }
}