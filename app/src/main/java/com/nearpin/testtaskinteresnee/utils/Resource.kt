package com.nearpin.testtaskinteresnee.utils

//a wrapper for transmitting data between model, viewModel and view
sealed class Resource<out T>(open val data: T?) {

    class Success<T>(override val data: T) : Resource<T>(data)

    class Loading<T>(data: T? = null) : Resource<T>(data)

    class Error<T>(val message: String, data: T? = null) : Resource<T>(data)

    fun <R> map(transform: (data: T) -> R): Resource<R> {
        val nullableValue: R? = data?.let { transform(it) }

        return when (this) {
            is Success -> Success(transform.invoke(data))
            is Loading -> Loading(nullableValue)
            is Error -> Error(message, nullableValue)
        }
    }
}