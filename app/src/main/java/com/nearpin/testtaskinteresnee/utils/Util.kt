package com.nearpin.testtaskinteresnee.utils

import androidx.fragment.app.Fragment
import androidx.navigation.navGraphViewModels

//viewModel provider
@androidx.annotation.MainThread
inline fun <reified VM : androidx.lifecycle.ViewModel> Fragment.defaultNavGraphViewModels(
    @androidx.annotation.IdRes navGraphId: Int
): Lazy<VM> = navGraphViewModels(navGraphId) {
    defaultViewModelProviderFactory
}