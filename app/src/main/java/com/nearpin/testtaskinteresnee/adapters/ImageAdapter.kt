package com.nearpin.testtaskinteresnee.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.nearpin.testtaskinteresnee.R
import com.nearpin.testtaskinteresnee.adapters.providers.ImageController
import com.nearpin.testtaskinteresnee.databinding.ImageItemBinding
import com.nearpin.testtaskinteresnee.models.Image
import timber.log.Timber

class ImageAdapter constructor(val imageController: ImageController): RecyclerView.Adapter<ImageAdapter.ViewHolder>() {
    class ViewHolder(val binding: ImageItemBinding): RecyclerView.ViewHolder(binding.root)

    private val items = ArrayList<Image>()

    fun update(list: ArrayList<Image>){
        items.clear()
        items.addAll(list)
        notifyDataSetChanged()
    }

    fun clear(){
        items.clear()
        notifyDataSetChanged()
    }

    //using the data binding for bind items
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ImageAdapter.ViewHolder {
        return ImageAdapter.ViewHolder(
                DataBindingUtil.inflate(
                        LayoutInflater.from(parent.context),
                        R.layout.image_item,
                        parent,
                        false
                )
        )
    }


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = items[position]
        Timber.tag("onBindViewHolder").d("${item}")

        //check are image object contain source to image
        if(item.original!=null)
            this.imageController.loadImage(holder.binding.imageView, item.original)

        holder.binding.container.setOnClickListener{
            imageController.openImage(position)
        }

    }

    override fun getItemCount() = items.size

}