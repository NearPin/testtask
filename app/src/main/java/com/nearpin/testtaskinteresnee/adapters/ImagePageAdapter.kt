package com.nearpin.testtaskinteresnee.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.nearpin.testtaskinteresnee.R
import com.nearpin.testtaskinteresnee.adapters.providers.ImagePageController
import com.nearpin.testtaskinteresnee.databinding.ImagePageBinding
import com.nearpin.testtaskinteresnee.models.Image

class ImagePageAdapter(val imageController: ImagePageController) : RecyclerView.Adapter<ImagePageAdapter.PagerViewHolder>(){
    class PagerViewHolder(val binding: ImagePageBinding) : RecyclerView.ViewHolder(binding.root)

    private val items = ArrayList<Image>()

    fun update(list: ArrayList<Image>){
        items.clear()
        items.addAll(list)
        notifyDataSetChanged()
    }

    //using data binding to bind items
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PagerViewHolder {
        return  ImagePageAdapter.PagerViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.image_page,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: PagerViewHolder, position: Int) {
        val item = items[position]
        holder.binding.image = item
        imageController.loadImage(holder.binding.imageView,item.original)
    }

    override fun getItemCount() = items.size
}