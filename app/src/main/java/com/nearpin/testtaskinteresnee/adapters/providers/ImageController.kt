package com.nearpin.testtaskinteresnee.adapters.providers

import android.widget.ImageView

//for image adapter with fragment
interface ImageController{
    fun openImage(position: Int)
    fun loadImage(imageView: ImageView, src: String)
}

//for page adapter with fragment
interface ImagePageController{
    fun openImage()
    fun loadImage(imageView: ImageView,src: String)
}