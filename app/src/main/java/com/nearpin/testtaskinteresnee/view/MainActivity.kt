package com.nearpin.testtaskinteresnee.view

import android.os.Bundle
import com.nearpin.testtaskinteresnee.R
import com.nearpin.testtaskinteresnee.base.ViewBindingActivity
import com.nearpin.testtaskinteresnee.databinding.ActivityMainBinding

//The main window of application. Using NavigationComponent to navigate from fragments
class MainActivity : ViewBindingActivity<ActivityMainBinding> (){
    override fun getLayoutId() = R.layout.activity_main

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }
}