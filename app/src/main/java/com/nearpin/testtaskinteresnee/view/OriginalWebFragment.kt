package com.nearpin.testtaskinteresnee.view

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebViewClient
import androidx.navigation.fragment.findNavController
import com.nearpin.testtaskinteresnee.R
import com.nearpin.testtaskinteresnee.base.ViewBindingFragment
import com.nearpin.testtaskinteresnee.databinding.FragmentOriginalWebBinding

//Fragment for check original page of image, using the WebView
class OriginalWebFragment : ViewBindingFragment<FragmentOriginalWebBinding>() {
    override fun getLayoutId() = R.layout.fragment_original_web

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val source = arguments?.getString("source")

        //give user possibility to do someone inside webview
        binding.webView.apply {
            webViewClient = WebViewClient()
            settings.javaScriptEnabled = true
            settings.loadWithOverviewMode = true
            settings.useWideViewPort = true
            loadUrl(source!!)
        }
        binding.include2.backButton.setOnClickListener { findNavController().popBackStack() }
    }
}