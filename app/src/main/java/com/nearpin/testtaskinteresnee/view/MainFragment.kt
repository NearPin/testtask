package com.nearpin.testtaskinteresnee.view

import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.Toast
import androidx.navigation.fragment.findNavController
import coil.api.load
import com.nearpin.testtaskinteresnee.R
import com.nearpin.testtaskinteresnee.adapters.ImageAdapter
import com.nearpin.testtaskinteresnee.adapters.providers.ImageController
import com.nearpin.testtaskinteresnee.base.ViewBindingFragment
import com.nearpin.testtaskinteresnee.databinding.FragmentMainBinding
import com.nearpin.testtaskinteresnee.utils.Resource
import com.nearpin.testtaskinteresnee.utils.defaultNavGraphViewModels
import com.nearpin.testtaskinteresnee.vm.MainFragmentViewModel

//Main fragment of app, that contain list of images with using recyclerview and gridlayout.
class MainFragment : ViewBindingFragment<FragmentMainBinding>(), ImageController {

    private val imageAdapter = ImageAdapter(this)


    val viewModel : MainFragmentViewModel by defaultNavGraphViewModels(R.id.SearchFragment)

    override fun getLayoutId() = R.layout.fragment_main

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.fragment = this

        binding.imageRecycler.adapter = imageAdapter

        //set click listeners
        prepareViews()

        //set observers
        prepareObservers()
    }

    private fun prepareViews(){
        binding.searchButton.setOnClickListener {startSearching()}
    }

    private fun startSearching(){
        viewModel.loadImages(binding.searchET.text.toString())
        imageAdapter.clear()
        binding.progressBar.visibility = View.VISIBLE
    }

    private fun stopSearching(){
        binding.progressBar.visibility = View.GONE
    }

    private fun prepareObservers(){

        viewModel.imagesLiveData.observe(viewLifecycleOwner,{
            when(it){
                is Resource.Loading -> {
                }
                is Resource.Success -> {
                    imageAdapter.update(it.data.imagesResults)
                    stopSearching()
                }
                is Resource.Error -> {
                    Toast.makeText(requireContext(), it.message, Toast.LENGTH_SHORT).show()
                    stopSearching()
                }
            }
        })

    }

    override fun openImage(position: Int) {
        val bundle = Bundle()
        bundle.putParcelable("searchResponse",viewModel.imagesLiveData.value!!.data)
        bundle.putInt("position",position)
        findNavController().navigate(R.id.action_SearchFragment_to_detailViewFragment,bundle)
    }

    override fun loadImage(imageView: ImageView, src: String) {
        //using Coil library to load image by source
        imageView.load(src)
    }
}