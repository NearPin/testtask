package com.nearpin.testtaskinteresnee.view

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ImageView
import androidx.navigation.fragment.findNavController
import coil.api.load
import com.nearpin.testtaskinteresnee.R
import com.nearpin.testtaskinteresnee.adapters.ImagePageAdapter
import com.nearpin.testtaskinteresnee.adapters.providers.ImageController
import com.nearpin.testtaskinteresnee.adapters.providers.ImagePageController
import com.nearpin.testtaskinteresnee.base.ViewBindingFragment
import com.nearpin.testtaskinteresnee.databinding.FragmentDetailViewBinding
import com.nearpin.testtaskinteresnee.models.Image
import com.nearpin.testtaskinteresnee.models.SearchResponse
import kotlinx.android.synthetic.main.fragment_detail_view.*


//Fragment for show detail photo and follow the link. User can swipe between photo to navigation.
//This fragment so small and dont need in viewmodel
class DetailViewFragment : ViewBindingFragment<FragmentDetailViewBinding>(), ImagePageController {
    override fun getLayoutId() = R.layout.fragment_detail_view

    val imageAdapter = ImagePageAdapter(this)

    val images = ArrayList<Image>()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initAdapter()
        initListeners()
    }

    private fun initListeners(){
        binding.include.backButton.setOnClickListener { findNavController().popBackStack() }
        binding.originalSourceButtonLayout.setOnClickListener { openImage() }
    }

    private fun initAdapter(){
        //initialize adapter to swipe images
        val searchResponse = arguments?.getParcelable<SearchResponse>("searchResponse")
        viewPager2.adapter = imageAdapter
        images.addAll(searchResponse!!.imagesResults)
        imageAdapter.update(images)
        val position = arguments?.getInt("position")

        //Must to set smoothScroll = false, because if viewPager will animate scroll, setCurrentItem() will work incorrect
        //for first four items. For example, try to set smoothScroll true and try to set current item = 1. Current item will be set to 0.
        //Similary for current item from 1 to 3
        binding.viewPager2.setCurrentItem(position!!,false)
    }

    override fun openImage() {
        val bundle = Bundle()
        bundle.putString("source",images[viewPager2.currentItem].link)
        findNavController().navigate(R.id.action_detailViewFragment_to_originalWebFragment,bundle)
    }


    override fun loadImage(imageView: ImageView, src: String) {
        //using coil library to load images
        imageView.load(src)
    }
}