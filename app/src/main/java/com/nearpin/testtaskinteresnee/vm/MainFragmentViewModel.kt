package com.nearpin.testtaskinteresnee.vm

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.gson.Gson
import com.nearpin.testtaskinteresnee.model.SearchRepository
import com.nearpin.testtaskinteresnee.models.SearchResponse
import com.nearpin.testtaskinteresnee.utils.Resource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch


class MainFragmentViewModel : ViewModel(){
    //live data for fragment
    val imagesLiveData =  MutableLiveData<Resource<SearchResponse>>(Resource.Loading())
    val api = SearchRepository()

    //using coroutines to do async requests
    fun loadImages(query: String){
        this.viewModelScope.launch (Dispatchers.IO){
            val res = api.search(query)
            //logging the result of request
            Log.e("The request result","${Gson().toJson(res.value?.data)}")
            imagesLiveData.postValue(res.value)
        }
    }

}