package com.nearpin.testtaskinteresnee.model

import androidx.lifecycle.MutableLiveData
import com.nearpin.testtaskinteresnee.models.SearchResponse
import com.nearpin.testtaskinteresnee.utils.Resource
import retrofit2.await

//the model part of architecture, there requests are being executed
class SearchRepository {

    suspend fun search(query: String): MutableLiveData<Resource<SearchResponse>> {
        val request = API.netService.search(query = query)
        val liveData = MutableLiveData<Resource<SearchResponse>>(Resource.Loading())

        try {
            val result = request.await()
            if(result.errorMessage==null)
                liveData.postValue(Resource.Success(result))
            else
                liveData.postValue(Resource.Error(result.errorMessage))

        }catch (ex: Exception){
            liveData.postValue(Resource.Error(ex.message.toString()))
        }

        return liveData
    }
}
