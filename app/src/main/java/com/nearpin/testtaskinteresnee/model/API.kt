package com.nearpin.testtaskinteresnee.model

import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import com.nearpin.testtaskinteresnee.models.SearchResponse
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.http.*

//using Retrofit2 library to do HTTP requests
object API {


    var API_BASE_URL: String = "https://serpapi.com";
    val API_KEY = "8e8744be59f2d4f27f0e4b40a445debde10ccb0e983d439430dd08efb99c3bdc"
    var httpClient = OkHttpClient.Builder().addInterceptor(
            HttpLoggingInterceptor().apply {
                HttpLoggingInterceptor.Level.BODY
            })

    var builder: Retrofit.Builder = Retrofit.Builder()
            .baseUrl(API_BASE_URL)
            .addCallAdapterFactory(CoroutineCallAdapterFactory())
            .addConverterFactory(GsonConverterFactory.create())

    var retrofit = builder
            .client(httpClient.build())
            .build()

    var netService = retrofit.create<NetService>(
            NetService::class.java!!)

    interface NetService {
        @GET("search.json")
        fun search(@Query("tbm") tbm : String = "isch",
                   @Query("api_key") apiKey: String = API_KEY,
                   @Query("q") query: String): Call<SearchResponse>
    }
}