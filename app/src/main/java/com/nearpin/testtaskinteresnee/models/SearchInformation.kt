package com.nearpin.testtaskinteresnee.models

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
class SearchInformation (

    @SerializedName("image_results_state")
    var imageResultsState: String,

    @SerializedName("query_displayed")
    var queryDisplayed: String,

    ): Parcelable