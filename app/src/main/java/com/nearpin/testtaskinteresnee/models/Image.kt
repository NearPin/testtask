package com.nearpin.testtaskinteresnee.models

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

//Object that contain information about images
@Parcelize
class Image(

      @SerializedName("position")
      val position: Int,

      @SerializedName("thumbnail")
      val thumbnail: String,

      @SerializedName("source")
      val source: String,

      @SerializedName("title")
      val title: String,

      @SerializedName("link")
      val link: String,

      @SerializedName("original")
      val original: String,

      @SerializedName("is_product")
      val isProduct: Boolean

): Parcelable