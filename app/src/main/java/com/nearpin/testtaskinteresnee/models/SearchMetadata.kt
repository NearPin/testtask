package com.nearpin.testtaskinteresnee.models

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
class SearchMetadata (

    @SerializedName("id")
    var id: String,

    @SerializedName("status")
    var status: String,

    @SerializedName("json_endpoint")
    var jsonEndPoint: String,

    @SerializedName("created_at")
    var createdAt: String,

    @SerializedName("google_url")
    var googleUrl: String,

    @SerializedName("total_time_taken")
    var totalTimeTaken: Float

    ): Parcelable