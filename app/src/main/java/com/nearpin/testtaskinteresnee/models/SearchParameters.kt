package com.nearpin.testtaskinteresnee.models

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
class SearchParameters(

    @SerializedName("engine")
    var engine: String,

    @SerializedName("q")
    var query: String,

    @SerializedName("google_domain")
    var googleDomain: String,

    @SerializedName("ijn")
    var ijn: Int,

    @SerializedName("device")
    var device: String,

    @SerializedName("tbm")
    var tbm: String

): Parcelable