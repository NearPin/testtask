package com.nearpin.testtaskinteresnee.models

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
class SearchResponse(

        @SerializedName("search_metadata")
    var searchMetadata: SearchMetadata,

        @SerializedName("search_parameters")
    var searchParameters: SearchParameters,

        @SerializedName("search_information")
    var searchInformation: SearchInformation,

        @SerializedName("error")
    var errorMessage: String,

        @SerializedName("images_results")
    var imagesResults: ArrayList<Image>,

        ): Parcelable