###
This is an application for viewing photos using Google search.
The user makes a request through the search bar in the toolbar, gets a list of photos in tabular form.
You can view the photos in full screen and swipe between them for navigation.
Just click on the "Original source" button to go to the original photo page
###